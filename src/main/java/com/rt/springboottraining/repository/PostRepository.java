package com.rt.springboottraining.repository;

import com.rt.springboottraining.model.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {

    @Query("select p from Post p")
    List<Post> findAllPosts(Pageable page);
    //pageable page - dodaje stronicowanie


  //  @Query("select p from Post p  left join fetch p.comment") //powinno rozwiazać problem n+1 zapytań, left join żeby pobrały się wszystkie posty,
    // List<Post> findAllPosts(Pageable page);                               //gdyby użyć inner join pobrałyby się tylko te z komentarzami
    //pageable page - dodaje stronicowanie



//    @Query("select p from Post p where title = :title")
//    List<Post> findAllByTitle(@Param("title") String title);


//    List<Post> findAllByTitle(String title);

//    @Query("select p from Post p where title = ?1")
//    List<Post> findAllByTitle(String title);
}
