package com.rt.springboottraining.service;

import com.rt.springboottraining.dto.PostDto;
import com.rt.springboottraining.model.Comment;
import com.rt.springboottraining.model.Post;
import com.rt.springboottraining.repository.CommentRepository;
import com.rt.springboottraining.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.awt.print.Pageable;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor //wstrzykiwanie repozytorium PostRepository przez konstruktor tworzony przez Lombok
public class PostService {

    private static final int PAGE_SIZE = 20;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    public List<Post> getPosts(Integer page, Sort.Direction sort){
        return postRepository.findAllPosts(
                PageRequest.of(page, PAGE_SIZE,
                    Sort.by(sort, "id"))); //pobiera stronę o indeksie 0, 5 postów, sortowanie
    }

    public Post getSinglePost(Long id) {
        return postRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
    }

    public List<Post> getPostsWithComments(Integer page, Sort.Direction sort) {
        List<Post> allPosts = postRepository.findAllPosts(PageRequest.of(page, PAGE_SIZE, Sort.by(sort, "id")));
        List<Long> ids = allPosts.stream()
                .map(Post::getId)
                .collect(Collectors.toList());
        List<Comment> comments = commentRepository.findAllByPostIdIsIn(ids);
        allPosts.forEach(post -> post.setComment(extractComments(comments, post.getId())));
        return allPosts;
    }

    private List<Comment> extractComments(List<Comment> comments, Long id) {
        return comments.stream()
                .filter(comment -> comment.getPostId() == id)
                .collect(Collectors.toList());
    }

    public Post addPost(Post post) {
        return postRepository.save(post);
    }

    @Transactional //adnotacja aby w metodzie mieć jedną transakcję
    public Post editPost(Post post) {
        Post postEdited = postRepository.findById(post.getId()).orElseThrow(NoSuchElementException::new);
        postEdited.setTitle(post.getTitle());
        postEdited.setContent(post.getContent());
        return postEdited;
    }

    public void deletePost(Long id) {
        postRepository.deleteById(id);
    }
}
